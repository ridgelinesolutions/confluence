# Atlassian Confluence in a Docker container

A containerized installation of Atlassian Confluence for use behind an ELB.

# Scheme

Container was designed to be run behind a load-balancer, with SSL terminated at the LB.

Container will re-write all http requests to https.

# Provision a Host

Confluence runs on port 8090 and port 8091.

The port 8090 connector will rewrite all requests to https, and includes proxy params.

The port 8091 connector serves http (for use with ELB health checks)

The CNAME, SL_DOMAIN, and TL_DOMAIN ENVs can be used to define the server FQDN for 
http-to-https rewrites.

Typical launch map host ports 80 and 81 to the two connectors.

```
docker run -d \
-e CNAME='confluence' \
-e SL_DOMAIN='mycompany' \
-e TL_DOMAIN='net' \
-v /var/local/atlassian/confluence:/var/local/atlassian/confluence:rw \
-v /etc/localtime:/etc/localtime:ro \
-p 80:8090 \
-p 81:8091 \
thomas_b_jackson/confluence:latest
```
# JVM Opts

## Performance 

Performance tuning is achieved primarily though the CATALINA_OPTS environment variable.

The following setpoint is appropriate for a large instance with 2-3K users, and should be paired with an VM with 32GB of RAM and at least 8 vCPU cores.

```
docker run -d \
-e CATALINA_OPTS="-Xms16G -Xmx16G -XX:NewSize=2048m -XX:+UseG1GC -XX:+DisableExplicitGC -XX:ReservedCodeCacheSize=128m -XX:+UseCodeCacheFlushing -Dconfluence.upgrade.recovery.file.enabled=false -Datlassian.plugins.enable.wait=600 -Dfile.encoding=UTF-8" \
-e CNAME='confluence' \
-e SL_DOMAIN='mycompany' \
-e TL_DOMAIN='net' \
-v /var/local/atlassian/confluence:/var/local/atlassian/confluence:rw \
-v /etc/localtime:/etc/localtime:ro \
-p 80:8090 \
-p 81:8091 \
thomas_b_jackson/confluence:latest

```
## Outbound Proxy

To navigate an outbound proxy, proxy configs must be added to CATALIN_OPTS, e.g.:

```
-Dhttp.proxySet=true \
-Dhttp.proxyHost=proxy.mycompany.net \
-Dhttp.proxyPort=3128 \
-Dhttps.proxyHost=proxy.mycompany.net \
-Dhttps.proxyPort=3128 \
-Dhttp.nonProxyHosts='localhost|10.0.0/8|*.mycompany.net' \
-Dhttps.nonProxyHosts='localhost|10.0.0/8|*.mycompany.net'
```

Combining this with the performance-related settings looks like:

```
docker run -d \
-e CATALINA_OPTS="-Xms16G -Xmx16G -XX:NewSize=2048m -XX:+UseG1GC -XX:+DisableExplicitGC -XX:ReservedCodeCacheSize=128m -XX:+UseCodeCacheFlushing -Dconfluence.upgrade.recovery.file.enabled=false -Datlassian.plugins.enable.wait=600 -Dfile.encoding=UTF-8 -verbose:gc -Dhttp.proxySet=true -Dhttp.proxyHost=pbcld-proxy.mycompany.net -Dhttp.proxyPort=3128 -Dhttps.proxyHost=pbcld-proxy.mycompany.net -Dhttps.proxyPort=3128 -Dhttp.nonProxyHosts='localhost|10.0.0/8|*.mycompany.net' -Dhttps.nonProxyHosts='localhost|10.0.0/8|*.mycompany.net'" \
-e CNAME='confluence' \
-e SL_DOMAIN='mycompany' \
-e TL_DOMAIN='net' \
-v /var/local/atlassian/confluence:/var/local/atlassian/confluence:rw \
-v /etc/localtime:/etc/localtime:ro \
-p 80:8090 \
-p 81:8091 \
thomas_b_jackson/confluence:latest
```



